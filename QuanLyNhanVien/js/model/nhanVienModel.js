const Staff = function (
  _account,
  _username,
  _email,
  _password,
  _day,
  _position,
  _salary,
  _time
) {
  this.account = _account;
  this.username = _username;
  this.email = _email;
  this.password = _password;
  this.day = _day;
  this.position = _position;
  this.salary = _salary;
  this.time = _time;
};
tinhXepLoai = function () {
  let xepLoai = "";
  if (this.time >= 192) {
    xepLoai = "Nhân viên xuất sắc";
  } else if (this.time >= 176) {
    xepLoai = "Nhân viên giỏi";
  } else if (this.time >= 160) {
    xepLoai = "Nhân viên khá";
  } else {
    xepLoai = "Nhân viên trung bình";
  }
  return xepLoai;
};
