function ValidatorStaff() {
  this.emptyCheck = function (idTarget, idError, messageError) {
    let valueTarget = document.getElementById(idTarget).value.trim();
    if (!valueTarget) {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
  this.checkValidAccount = function (idTarget, idError) {
    let pattern = /^[0-9]{4,6}$/;
    let valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Tài khoản phải là số, dài 4-6 ký tự";
      return false;
    }
  };
  this.checkValidName = function (idTarget, idError) {
    let pattern = /[a-zA-Z]+/g;
    let valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = "Tên nhân viên phải là chữ";
      return false;
    }
  };
  this.checkValidEmail = function (idTarget, idError) {
    let pattern =
      /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
    let valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = "Email không hợp lệ";
      return false;
    }
  };
  this.checkValidPassword = function (idTarget, idError) {
    let pattern =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;
    let valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Mật khẩu dài từ 6 - 10 ký tự (có ít nhất 1 ký tự số, 1 ký tự in hoa và 1 ký tự đặc biệt)";
      return false;
    }
  };
  this.checkValidDate = function (idTarget, idError) {
    let pattern = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    let valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Định dạng không hợp lệ (MM-DD-YYYY)";
      return false;
    }
  };
  this.checkValidSalary = function (idTarget, idError) {
    let pattern = /^([1-9]\d{6}|1\d{7}|20{7})$/;
    let valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Tối thiểu là 1.000.000, tối đa là 20.000.000";
      return false;
    }
  };
  this.checkValidPosition = function (idTarget, idError) {
    let pattern = /^(?!blank$).*/;
    let valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = "Vui lòng chọn chức vụ";
      return false;
    }
  };
  this.checkValidTime = function (idTarget, idError) {
    let pattern = /^([8-9]\d{1}|1\d{2}|200)$/;
    let valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Giờ làm trong tháng tối thiểu là 80h, tối đa là 200h";
      return false;
    }
  };
}
