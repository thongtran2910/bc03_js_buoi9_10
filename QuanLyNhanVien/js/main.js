let staffList = [];
let validatorStaff = new ValidatorStaff();

const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

const findPosition = (id, array) => {
  return array.findIndex((nv) => {
    return nv.account == id;
  });
};

const saveLocalStorage = () => {
  let dsnvJson = JSON.stringify(staffList);
  localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
};
let dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
if (dsnvJson) {
  staffList = JSON.parse(dsnvJson);
  renderStaffList(staffList);
}

document.getElementById("btnThemNV").addEventListener("click", () => {
  let newStaff = getFormInfo();
  if (isValid()) {
    staffList.push(newStaff);
    renderStaffList(staffList);
    $("#myModal").modal("hide");
    document.getElementById("staff-form").reset();
    saveLocalStorage();
  } else {
    showErrorMessage();
  }
});

const delStaff = (account) => {
  let index = findPosition(account, staffList);
  staffList.splice(index, 1);
  renderStaffList(staffList);
  saveLocalStorage();
};

const editStaff = (account) => {
  document.getElementById("btnCapNhat").style.display = "block";
  document.getElementById("btnThemNV").style.display = "none";
  let index = findPosition(account, staffList);
  let staff = staffList[index];
  renderListToForm(staff);
  $("#myModal").modal("show");
};

document.getElementById("btnCapNhat").addEventListener("click", () => {
  let editedStaff = getFormInfo();
  if (isValid()) {
    let index = findPosition(editedStaff.account, staffList);
    staffList[index] = editedStaff;
    renderStaffList(staffList);
    $("#myModal").modal("hide");
    document.getElementById("staff-form").reset();
    saveLocalStorage();
  } else {
    showErrorMessage();
  }
});
document.getElementById("btnThem").addEventListener("click", () => {
  document.getElementById("btnThemNV").style.display = "block";
  document.getElementById("btnCapNhat").style.display = "none";
  document.getElementById("staff-form").reset();
});

document.getElementById("btnDong").addEventListener("click", () => {
  document.getElementById("staff-form").reset();
});
