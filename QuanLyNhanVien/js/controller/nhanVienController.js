function getFormInfo() {
  let tkNhanVien = document.getElementById("tknv").value;
  let tenNv = document.getElementById("name").value;
  let emailNv = document.getElementById("email").value;
  let matKhauNv = document.getElementById("password").value;
  let ngayLam = document.getElementById("datepicker").value;
  let chucVu = document.getElementById("chucvu").value;
  let luongCb = document.getElementById("luongCB").value;
  let gioLam = document.getElementById("gioLam").value * 1;

  let staff = new Staff(
    tkNhanVien,
    tenNv,
    emailNv,
    matKhauNv,
    ngayLam,
    chucVu,
    luongCb,
    gioLam
  );
  return staff;
}

function renderStaffList(list) {
  let contentHTML = "";
  for (let i = 0; i < list.length; i++) {
    getFormInfo();
    let staff = list[i];
    let contentTr = `<tr>
    <td>${staff.account}</td>
    <td>${staff.username}</td>
    <td>${staff.email}</td>
    <td>${staff.day}</td> 
    <td>${staff.position}</td>
    <td>${staff.salary}</td>
    <td>${staff.tinhXepLoai}</td>
    <td>
    <button onclick="editStaff('${staff.account}')" class="btn btn-info">Sửa</button>
    <button onclick="delStaff('${staff.account}')" class="btn btn-danger">Xóa</button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function renderListToForm(nv) {
  document.getElementById("tknv").value = nv.account;
  document.getElementById("name").value = nv.username;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.password;
  document.getElementById("datepicker").value = nv.day;
  document.getElementById("chucvu").value = nv.position;
  document.getElementById("luongCB").value = nv.salary;
  document.getElementById("gioLam").value = nv.time;
}

function isValid() {
  let isValid = true;
  let isValidAccount =
    validatorStaff.emptyCheck(
      "tknv",
      "tbTKNV",
      "Tài khoản không được bỏ trống"
    ) && validatorStaff.checkValidAccount("tknv", "tbTKNV");
  let isValidName =
    validatorStaff.emptyCheck(
      "name",
      "tbTen",
      "Tên nhân viên không được bỏ trống"
    ) && validatorStaff.checkValidName("name", "tbTen");
  let isValidEmail =
    validatorStaff.emptyCheck(
      "email",
      "tbEmail",
      "Email nhân viên không được bỏ trống"
    ) && validatorStaff.checkValidEmail("email", "tbEmail");
  let isValidPassword =
    validatorStaff.emptyCheck(
      "password",
      "tbMatKhau",
      "Mật khẩu không được bỏ trống"
    ) && validatorStaff.checkValidPassword("password", "tbMatKhau");
  let isValidDate =
    validatorStaff.emptyCheck(
      "datepicker",
      "tbNgay",
      "Ngày làm không được bỏ trống"
    ) && validatorStaff.checkValidDate("datepicker", "tbNgay");
  let isValidSalary =
    validatorStaff.emptyCheck(
      "luongCB",
      "tbLuongCB",
      "Lương cơ bản không được bỏ trống"
    ) && validatorStaff.checkValidSalary("luongCB", "tbLuongCB");
  let isValidPosition = validatorStaff.checkValidPosition("chucvu", "tbChucVu");
  let isValidTime =
    validatorStaff.emptyCheck(
      "gioLam",
      "tbGiolam",
      "Giờ làm không được bỏ trống"
    ) && validatorStaff.checkValidTime("gioLam", "tbGiolam");
  return (isValid =
    isValidAccount &&
    isValidName &&
    isValidEmail &&
    isValidPassword &&
    isValidDate &&
    isValidSalary &&
    isValidPosition &&
    isValidTime);
}

function showErrorMessage() {
  document.getElementById("tbTKNV").style.display = "block";
  document.getElementById("tbTen").style.display = "block";
  document.getElementById("tbEmail").style.display = "block";
  document.getElementById("tbMatKhau").style.display = "block";
  document.getElementById("tbNgay").style.display = "block";
  document.getElementById("tbChucVu").style.display = "block";
  document.getElementById("tbLuongCB").style.display = "block";
  document.getElementById("tbGiolam").style.display = "block";
}
